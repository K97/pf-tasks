function validationEmail(){
    var mailEle = document.getElementById("email");
    mailEle.className = "";
    if( /(.+)@(.+){2,}\.(.+){2,}/.test(mailEle.value) ){
        mailEle.className = "";
    } else {
     mailEle.className += "invalid";
 }
}

function postFormVal(form){
    form.reset();
    document.getElementById('success-msg').style.display = 'block';
    return false;
}
